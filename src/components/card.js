import React from "react";
import { useSelector, useDispatch } from "react-redux";

const Card = (props) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.cardData.cardData);
  console.log(data);

  const isLikedBtnHandler = (id) => {
  };
  return (
    <>
      <div className="row">
        {props.data.map((res) => {
          return (
            <div
              className="card"
              style={{ width: "26rem", margin: "19px" }}
              key={res.id}
            >
              <img
                src={`https://avatars.dicebear.com/v2/avataaars/${res.username}.svg?options[mood][]=happy`}
                className="card-img-top"
                alt="..."
                style={{
                  width: "200px",
                  height: "200px",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
              <div className="card-body">
                <div>
                  <h3>{res.name}</h3>

                  <ul
                    style={{
                      fontSize: "20px",
                      margin: "0px",
                      padding: "0px 0px",
                    }}
                    className="list-unstyled"
                  >
                    <li style={{ margin: "0px", padding: "0px 0px 0px 0px" }}>
                      <i
                        className="far fa-envelope"
                        style={{ paddingRight: "15px" }}
                      />
                      {res.email}
                    </li>
                    <li style={{ margin: "0px", padding: "0px 0px 0px 0px" }}>
                      <i
                        className="fas fa-phone"
                        style={{ paddingRight: "15px" }}
                      />
                      {res.phone}
                    </li>
                    <li style={{ margin: "0px", padding: "0px 0px 0px 0px" }}>
                      <i
                        className="fas fa-globe-asia"
                        style={{ paddingRight: "15px" }}
                      />
                      http://{res.website}
                    </li>
                  </ul>
                </div>

                <hr />

                <div>
                  <button
                    style={{
                      background: "none",
                      border: "none",
                      cursor: "pointer",
                      outline: "none",
                      float: "left",
                    }}
                    onClick={() => isLikedBtnHandler(res.id)}
                  >
                    <i
                      className={`${
                        res.isLiked ? "fas" : "far"
                      } fa-heart fa-2x`}
                      style={{ color: "red" }}
                    />
                  </button>
                  <button
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    style={{
                      background: "none",
                      border: "none",
                      cursor: "pointer",
                      outline: "none",
                      marginLeft: "36%",
                    }}
                  >
                    <span className="fas fa-user-edit  fa-2x" />
                  </button>

                  <button
                    type="button"
                    style={{
                      background: "none",
                      border: "none",
                      cursor: "pointer",
                      outline: "none",
                      float: "right",
                    }}
                  >
                    <span className="fas fa-trash fa-2x" />
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>

      {/* //-------------------Edit-Modal----------------------- */}
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Edit User Data
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="mb-3 row">
                <label className="col-sm-2 col-form-label ">
                  <span style={{ color: "red" }}>*</span>Name:{" "}
                </label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="mb-3 row">
                <label className="col-sm-2 col-form-label">
                  <span style={{ color: "red" }}>*</span>Email:{" "}
                </label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="mb-3 row">
                <label className="col-sm-2 col-form-label">
                  <span style={{ color: "red" }}>*</span>Phone:{" "}
                </label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="mb-3 row">
                <label className="col-sm-2 col-form-label">
                  <span style={{ color: "red" }}>*</span>Website:{" "}
                </label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                OK
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Card;
