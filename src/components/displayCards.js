import React, { useEffect, useState } from "react";
import fetchUsersData from "./api";
import { useDispatch, useSelector } from "react-redux";
import Card from "./card";
import axios from "axios";
import { setCardData } from "../Redux/action/cardData";

const DisplayCards = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.cardData.cardData);
  // const [users, setUsers] = useState([]);

  // const fetchUsersData = await dispatch(fetchUsersData());
  useEffect(async () => {
    const fetch = async () => {
      let url = "https://jsonplaceholder.typicode.com/users";
      try {
        const { data } = await axios.get(url);
        return await data;
      } catch (error) {
        alert(error.message);
      }
    };

    const data = await fetch();
    // dispatch(setCardData(data));
    // setUsers(data);
    // const dataa = fetchData();
    // console.log(dataa);
  }, [dispatch]);
  console.log(users, "redux");
  return (
    <>
      <Card data={users} />
    </>
  );
};

export default DisplayCards;
