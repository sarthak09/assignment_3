import axios from "axios";

const fetchUsersData = async () => {
  let url = "https://jsonplaceholder.typicode.com/users";
  try {
    const { data } = await axios.get(url);
    return data;
  } catch (error) {
    alert(error.message);
  }
};
export default fetchUsersData;
