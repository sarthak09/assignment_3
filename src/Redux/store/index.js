import { createStore } from "redux";
import rootReducer from "../reducer/rootReduser";
const store = createStore(rootReducer);

export default store;
