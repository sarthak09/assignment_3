export const setCardData = (data) => {
    return (dispatch) => {
      dispatch({ type: "SET_CARD_DATA", data: data });
    };
  };
  