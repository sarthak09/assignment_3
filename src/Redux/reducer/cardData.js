const initialState = {
  cardData: {},
};
const cardDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_CARD_DATA":
      return { ...state, cardData: action.data };
    default:
      return state;
  }
};

export default cardDataReducer;
