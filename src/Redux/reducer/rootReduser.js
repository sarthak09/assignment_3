// ** Redux Imports
import { combineReducers } from "redux";

// ** Reducers Imports
import cardData from "./cardData";

const rootReducer = combineReducers({
  cardData,
});

export default rootReducer;
